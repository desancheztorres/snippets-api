<?php

Route::get('/', function () {
    dd('hello world!');
});

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('signin', 'SigninController');
    Route::get('me', 'MeController');
    Route::post('signout', 'SignOutController');
});

Route::group(['prefix' => 'snippets', 'namespace' => 'Snippets'], function () {
    Route::post('', 'SnippetController@store');
    Route::get('{snippet}', 'SnippetController@show');
    Route::patch('{snippet}', 'SnippetController@update');

    Route::group(['prefix' => '{snippet}/steps/'], function () {
        Route::post('', 'StepController@store');
        Route::patch('{step}', 'StepController@update');
        Route::delete('{step}', 'StepController@destroy');
    });
});
