<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Snippet extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['uuid', 'title', 'is_public'];

    /**
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function(Snippet $snippet) {
            $snippet->uuid = Str::uuid();
        });

        static::created(function(Snippet $snippet) {
            $snippet->steps()->create([
                'order' => 1
            ]);
        });
    }

    /**
     * @return mixed
     */
    public function isPublic() {
        return $this->is_public;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function steps() {
        return $this->hasMany(Step::class)
            ->orderBy('order', 'asc');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
