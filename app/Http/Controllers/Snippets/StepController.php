<?php

namespace App\Http\Controllers\Snippets;

use App\Http\Controllers\Controller;
use App\Http\Requests\Snippets\UpdateStepRequest;
use App\{Snippet, Step, Transformers\Snippets\StepTransformer};
use Illuminate\Http\Request;

class StepController extends Controller
{
    /**
     * @param UpdateStepRequest $request
     * @param Snippet $snippet
     * @return array
     */
    public function store(UpdateStepRequest $request, Snippet $snippet) {

        $this->authorize('storeStep', $snippet);

        $step = $snippet->steps()->create(
            array_merge(
                $request->only('title', 'body'),
                ['order' => $this->getOrder($request)]
            )
        );

        return fractal()
            ->item($step)
            ->transformWith(new StepTransformer)
            ->toArray();
    }

    /**
     * @param UpdateStepRequest $request
     * @param Snippet $snippet
     * @param Step $step
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateStepRequest $request, Snippet $snippet, Step $step) {

        $this->authorize('update', $step);

        $step->update($request->only('title', 'body'));

        return fractal()
            ->item($step)
            ->transformWith(new StepTransformer)
            ->toArray();
    }

    /**
     * @param Snippet $snippet
     * @param Step $step
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Snippet $snippet, Step $step) {

        $this->authorize('destroy', $step);

        if($snippet->steps->count() === 1) {
            return response(null, 400);
        }

        $step->delete();

        return response(null, 204);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    protected function getOrder(Request $request) {
        return Step::where('uuid', $request->before)
            ->orWhere('uuid', $request->after)
            ->first()
            ->{($request->before ? 'before' : 'after') . 'Order'}();
    }
}
