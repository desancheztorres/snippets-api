<?php

namespace App\Http\Controllers\Snippets;

use App\Http\Controllers\Controller;
use App\Http\Requests\Snippets\UpdateSnippetRequest;
use App\Snippet;
use App\Transformers\Snippets\SnippetTransformer;
use Illuminate\Auth\Access\AuthorizationException as AuthorizationExceptionAlias;
use Illuminate\Http\Request;

class SnippetController extends Controller
{
    /**
     * SnippetController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth:api'])
            ->only('store', 'update');
    }

    /**
     * @param Snippet $snippet
     * @return array
     * @throws AuthorizationExceptionAlias
     */
    public function show(Snippet $snippet) {

        $this->authorize('show', $snippet);

        return fractal()
            ->item($snippet)
            ->parseIncludes(['steps', 'author', 'user'])
            ->transformWith(new SnippetTransformer())
            ->toArray();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function store(Request $request) {
        $snippet = $request->user()->snippets()->create();

        return fractal()
            ->item($snippet)
            ->transformWith(new SnippetTransformer())
            ->toArray();
    }

    /**
     * @param UpdateSnippetRequest $request
     * @param Snippet $snippet
     * @return array
     * @throws AuthorizationExceptionAlias
     */
    public function update(UpdateSnippetRequest $request, Snippet$snippet) {

        $this->authorize('update', $snippet);

        $snippet->update($request->only('title', 'is_public'));

        return fractal()
            ->item($snippet)
            ->parseIncludes(['steps', 'author', 'user'])
            ->transformWith(new SnippetTransformer())
            ->toArray();
    }
}
